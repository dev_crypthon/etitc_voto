# Votaciones con tecnología blockchain

Plataforma dígital que permite la elección de representantes y directivos en instituciones que eligen por mecanismo popular (voto), para garantizar elecciones a distancia, transparentes, seguras y con resultados inmediatos mediante un sistema blockchain. 

## Muckup

https://www.figma.com/proto/bq5AKbBuYL6DwrPIRWij4B/Crypthon?node-id=5%3A2&scaling=scale-down

## Presentación del proyecto
https://www.slideshare.net/DavidEstebanFajardoT/crypthon-presentacin

## Vídeo de presentación
https://youtu.be/KZoFu2LJ4ag


For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Para conocer más del proyecto puedes consultar en https://crypthon.tk/ o escribirnos al correo crypthon4@gmail.com


 > *Transacción a transacción se logra la elección*
