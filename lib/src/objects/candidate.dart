class Candidate {
  final int id;
  final String image;
  final String name;
  final String description;

  final bool error;
  final String errorDetails;

  Candidate(this.id, this.image, this.name, this.description)
      : this.error = false,
        this.errorDetails = null;

  factory Candidate.fromJson(Map<String, dynamic> json) {
    return Candidate(
        json["id"], json["image"], json["name"], json["description"]);
  }

  Candidate.error(this.errorDetails)
      : this.error = true,
        this.id = null,
        this.image = null,
        this.name = null,
        this.description = null;
}
