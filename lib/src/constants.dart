import 'package:flutter/material.dart';

class Constants {
  static final Color primaryColor = Color(0xFF72C74D);
  static final Color primaryDark = Color(0xFF3E961B);
  static final Color primaryLight = Color(0xFFA5fB7D);
  static final Color accentColor = Color(0xFFABACB0);

  static final String apiKey =
      "AAAAwM73Qec:APwhfgr9gQHJnNdEfh0EpsoDenKNulf4OfdkjhQySGUYpstMfNu7JglD9KlS1ZLsbcvf45hgf458NYB4pQzsAtn6dmCvVUtKVlg-yDVp7SYkAF8CN6yNXq_fzIBrLzi5oZexs04R-L6rg";

  static final String rootUrl = "https://blockchain.juandavid.dev";

  static var candidatesUrl = "$rootUrl/candidates";

  static final siriusBaseUrl = 'http://bctestnet2.brimstone.xpxsirius.io:3000';
}
