import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircularLoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget indicator;

    if (Platform.isIOS) {
      indicator = CupertinoActivityIndicator();
    } else {
      indicator = CircularProgressIndicator();
    }

    return indicator;
  }
}