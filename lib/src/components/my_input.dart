import 'package:etitc_voto/src/constants.dart';
import 'package:flutter/material.dart';

class MyInput extends StatelessWidget {
  final bool autofocus;
  final String title;

  MyInput(this.autofocus, this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 45,
      child: TextField(
        autofocus: this.autofocus,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          labelText: this.title,
          labelStyle: TextStyle(color: Constants.primaryDark),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Constants.primaryDark),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Constants.primaryDark),
          ),
        ),
      ),
    );
  }
}
