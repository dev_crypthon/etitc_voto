import 'package:etitc_voto/src/constants.dart';
import 'package:xpx_chain_sdk/xpx_sdk.dart';

class XPXService {
  /**
   * @return bool true if execution is ok
   */
  static Future<bool> vote(
      String originAccount, String recipientAccount, String voteMessage) async {
    final client = SiriusClient.fromUrl(Constants.siriusBaseUrl, null);
    final generationHash = await client.generationHash;
    final networkType = await client.networkType;

    final account = Account.fromPrivateKey(originAccount, networkType);

    final recipient = Address.fromRawAddress(recipientAccount);

    final tx = TransferTransaction(Deadline(hours: 1), recipient, [xpx(10)],
        PlainMessage(payload: voteMessage), networkType);

    final stx = account.signTransaction(tx, generationHash);

    try {
      final restTx = await client.transaction.announce(stx);
      print(restTx);
      print('Hash: ${stx.hash}');
      print('Signer: ${account.publicAccount.publicKey}');
      return true;
    } on Exception catch (e) {
      print('Exception when calling Transaction->Announce: $e\n');
      return false;
    }
  }
}
