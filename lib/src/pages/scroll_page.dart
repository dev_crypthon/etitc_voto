import 'package:etitc_voto/src/pages/wallet_page.dart';
import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        _pagina1(),
        _pagina2(context),
      ],
    ));
  }

  Widget _pagina1() {
    return Stack(
      children: <Widget>[
        _colorFondo(),
        _imagenFondo(),
        _textos(),
      ],
    );
  }

  Widget _colorFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(255, 255, 255, 1.0),
    );
  }

  Widget _imagenFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/bgOne.png'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _textos() {
    final estiloTexto = TextStyle(
        color: Color.fromRGBO(255, 255, 255, 1),
        fontSize: 50.0,
        fontWeight: FontWeight.bold);
    final estiloFecha =
        TextStyle(color: Color.fromRGBO(255, 255, 255, 1), fontSize: 30.0);

    return SafeArea(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20.0,
          ),
          Text('ETITC', style: estiloTexto),
          Text('Votaciones', style: estiloTexto),
          Text('2021-1', style: estiloFecha),
          Expanded(
            child: Container(),
          ),
          Icon(Icons.keyboard_arrow_down, size: 70.0, color: Colors.white)
        ],
      ),
    );
  }

  Widget _pagina2(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(255, 255, 255, 1.0),
      child: Center(
        child: RaisedButton(
          shape: StadiumBorder(),
          color: Color.fromRGBO(38, 120, 60, 0.8),
          textColor: Colors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
            child: Text(
              'Ingresar identificación',
              style: TextStyle(fontSize: 20.0),
            ),
          ),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => WalletPage()));
          },
        ),
      ),
    );
  }
}
