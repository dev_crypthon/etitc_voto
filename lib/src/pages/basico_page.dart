import 'package:flutter/material.dart';

class BasicoPage extends StatelessWidget {

  final estiloTitulo = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );
  //const BasicoPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
        children: <Widget>[
        _crearImagen(),
        _crearTitulo(),
        _crearAcciones(),
        _crearTexto(),
        _crearTexto(),
        _crearTexto(),
        _crearTexto(),
        _crearTexto(),
        ],

      ),
    ),
   );
  }

  Widget _crearImagen(){
    return Image(
          image: NetworkImage('https://i.pinimg.com/originals/62/dd/dd/62dddd03bab522af0b94fa33fa1fa45d.jpg'),
    );
  }

  Widget _crearTitulo(){

    return SafeArea(
          child: Container(
                 padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
                 child: Row(
                   children: <Widget>[

                     Expanded(
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                           Text('God of war', style: estiloTitulo),
                           SizedBox( height:  7.0),
                           Text('Dios de la guerra', style: estiloSubTitulo)
                         ],
                         ),
                     ),

                       Icon( Icons.star, color: Colors.red, size: 30.0),
                       Text('4.1', style: TextStyle(fontSize: 20.0 ),)

                   ],
                 ),
               ),
    );

  }

  Widget _crearAcciones(){

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[

        _accion(Icons.call, 'CALL'),
        _accion(Icons.near_me, 'ROUTE'),
        _accion(Icons.share, 'SHARE'),

      ],
      );

  }

  Widget _accion(IconData icon, String texto){

    return Column(
      children: <Widget>[
        Icon( icon, color: Colors.blue, size: 40.0, ),
        SizedBox( height: 5.0,),
        Text( texto, style: TextStyle( fontSize:  15.0, color: Colors.blue ), ),
      ],
    );

  }

  Widget _crearTexto(){

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 40.0),
        child: Text(
          'Ipsum nostrud tempor proident ipsum consectetur enim. Id sunt nostrud velit sunt duis anim officia adipisicing nostrud consequat ipsum cillum commodo adipisicing. Laborum anim Lorem cillum nostrud sunt ex. Occaecat pariatur officia excepteur ullamco ut mollit sit. Ut enim officia aliqua excepteur eu proident ex consectetur. Cillum consectetur cillum culpa et irure.',
          textAlign: TextAlign.justify,
        ),
      ),
    );
  }
}